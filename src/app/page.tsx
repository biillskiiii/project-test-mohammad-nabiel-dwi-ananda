"use client"
import { useEffect, useState } from "react";
import Navbar from "@/components/navbar";
import axios from "axios";
import Image from "next/image";
import Hero from "../../public/assets/hero.jpg";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";

type Idea = {
  id: number;
  title: string;
  published_at: string;
  small_image: string;
};

const Home = () => {
  const [ideas, setIdeas] = useState<Idea[]>([]);
  const [page, setPage] = useState<number>(1);
  const [totalPages, setTotalPages] = useState<number>(1);
  const [pageSize, setPageSize] = useState<number>(10);
  const [sortOrder, setSortOrder] = useState<string>("desc");

  useEffect(() => {
    fetchData();
  }, [page, pageSize, sortOrder]);

  const fetchData = async () => {
    try {
      const response = await axios.get(
        "https://suitmedia-backend.suitdev.com/api/ideas",
        {
          params: {
            "page[number]": page,
            "page[size]": pageSize,
            sort: sortOrder === "desc" ? "-published_at" : "published_at",
            append: "small_image",
          },
        }
      );
      if (response.status === 200) {
        if (response.data && Array.isArray(response.data.data)) {
          const formattedIdeas: Idea[] = response.data.data.map(
            (idea: any) => ({
              id: idea.id,
              title: idea.title,
              published_at: idea.published_at,
              small_image: idea.small_image,
            })
          );
          setIdeas(formattedIdeas);
          setTotalPages(Math.ceil(response.data.meta.total / pageSize));
        } else {
          console.error("Invalid data structure returned:", response.data);
        }
      } else {
        console.error("Failed to fetch data:", response.statusText);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const formatDateString = (dateString: string) => {
    const options: Intl.DateTimeFormatOptions = {
      day: "numeric",
      month: "long",
      year: "numeric",
    };
    return new Date(dateString).toLocaleDateString(undefined, options);
  };

  const getPageNumbers = () => {
    const maxPagesToShow = 3;
    const halfMaxPagesToShow = Math.floor(maxPagesToShow / 2);
    let startPage = Math.max(1, page - halfMaxPagesToShow);
    let endPage = startPage + maxPagesToShow - 1;

    if (endPage > totalPages) {
      endPage = totalPages;
      startPage = Math.max(1, endPage - maxPagesToShow + 1);
    }

    const pageNumbers = [];
    for (let i = startPage; i <= endPage; i++) {
      pageNumbers.push(i);
    }

    return pageNumbers;
  };

  return (
    <div className="overflow-hidden">
      <div className="absolute top-0 left-0 w-full z-50">
        <Navbar />
      </div>
      <div className="object-cover -rotate-6 mb-24 top-0 h-[650px] scale-125  overflow-hidden">
        <div className="relative h-full w-11/12 -top-32 transform overflow-hidden">
          <Image
            src={Hero}
            alt="hero-img"
            layout="fill"
            objectFit="cover"
            className="opacity-75"
          />
          <div className="absolute inset-0 bg-black opacity-75"></div>
        </div>
        <div className="absolute top-0 inset-0 rotate-6 z-10 flex flex-col items-center justify-center text-white">
          <div className="container mx-auto text-center p-4">
            <h1 className="text-5xl font-bold mb-2">Ideas</h1>
            <p className="text-2xl">Where all our great things begin</p>
          </div>
        </div>
      </div>

      <div className="container mx-auto px-20 py-8">
        <div className="flex justify-between items-center mb-4">
          <div>
            Showing {page} - {pageSize} of 100
          </div>
          <div className="flex items-center space-x-4">
            <label htmlFor="pageSize">Items per page:</label>
            <select
              id="pageSize"
              value={pageSize}
              onChange={(e) => {
                setPageSize(parseInt(e.target.value));
                setPage(1); // Reset page to 1 when page size changes
              }}
              className="border rounded px-2 py-1"
            >
              <option value={5}>5</option>
              <option value={10}>10</option>
              <option value={20}>20</option>
            </select>

            <label htmlFor="sortOrder">Sort by:</label>
            <select
              id="sortOrder"
              value={sortOrder}
              onChange={(e) => setSortOrder(e.target.value)}
              className="border rounded px-2 py-1"
            >
              <option value="desc">Newest</option>
              <option value="asc">Oldest</option>
            </select>
          </div>
        </div>
        <div className="flex flex-wrap -mx-4">
          {ideas.map((idea) => (
            <div key={idea.id} className="w-11/12 sm:w-1/2 lg:w-1/4 px-4 mb-8">
              <div className="border w-full rounded-lg h-80 shadow-lg">
                <div className="relative w-full h-48">
                  <Image
                    src={idea.small_image}
                    alt={idea.title}
                    layout="fill"
                    objectFit="cover"
                    className="rounded-t-lg"
                    loading="lazy"
                  />
                </div>
                <div className="px-4 pb-6"> {/* Added pb-6 for bottom padding */}
                  <p className="text-gray-500 text-sm mb-2">
                    {formatDateString(idea.published_at)}
                  </p>
                  <h2 className="text-lg font-semibold mb-2 overflow-hidden text-ellipsis line-clamp-3">
                    {idea.title}
                  </h2>
                </div>
              </div>
            </div>
          ))}
        </div>

        <div className="flex justify-center mt-8">
          <button
            disabled={page === 1}
            onClick={() => setPage(page - 1)}
            className="px-3 py-1 mx-1 border rounded"
          >
            <IoIosArrowBack size={15} />
          </button>
          {getPageNumbers().map((pageNumber) => (
            <button
              key={pageNumber}
              onClick={() => setPage(pageNumber)}
              className={`px-3 py-1 mx-1 border rounded ${
                page === pageNumber ? "bg-orange-500 text-white" : ""
              }`}
            >
              {pageNumber}
            </button>
          ))}
          <button
            disabled={page === totalPages}
            onClick={() => setPage(page + 1)}
            className="px-3 py-1 mx-1 border rounded"
          >
            <IoIosArrowForward size={15} />
          </button>
        </div>
      </div>
    </div>
  );
};

export default Home;
